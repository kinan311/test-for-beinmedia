import { createMuiTheme } from '@material-ui/core/styles';



const theme = createMuiTheme({
    typography: {
        fontFamily: ''// specifying a new font
    },
    overrides: {
        MuiCssBaseline: {
          boxShadow: '0px 8px 10px -5px rgb(255 255 255 / 20%), 0px 16px 24px 2px rgb(255 255 255 / 14%), 0px 6px 30px 5px rgb(255 255 255 / 12%) !important'
          
        },
        paper: {
         boxShadow: '0px 8px 10px -5px rgb(255 255 255 / 20%), 0px 16px 24px 2px rgb(255 255 255 / 14%), 0px 6px 30px 5px rgb(255 255 255 / 12%) !important'
   
        }
    },
});

export default theme;