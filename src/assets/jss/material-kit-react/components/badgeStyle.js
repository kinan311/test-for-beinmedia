import {
  primaryColor,
  warningColor,
  dangerColor,
  successColor,
  infoColor,
  blueColor,
  transperansy
} from "assets/jss/material-kit-react.js";

const badgeStyle = {
  badge: {
    marginRight: "3px",
    borderRadius: "12px",
    padding: "5px",
    textTransform: "uppercase",
    fontSize: "10px",
    fontWeight: "500",
    lineHeight: "1",
    color: "#fff",
    textAlign: "center",
    whiteSpace: "nowrap",
    verticalAlign: "baseline",
    display: "inline-block"
  },
  primary: {
    backgroundColor: primaryColor
  },
  warning: {
    backgroundColor: warningColor
  },
  danger: {
    backgroundColor: dangerColor
  },
  success: {
    backgroundColor: successColor
  },
  info: {
    backgroundColor: infoColor
  },
  blue: {
    backgroundColor: blueColor
  },
  gray: {
    backgroundColor: "#6c757d"
  },
  transperansy: {
    backgroundColor: transperansy,
    color: "#1d428b",
    fontSize: '20px',
    fontWeight: '700',
    justifyContent: 'center',
    alignItems: 'center',
    display: 'flex',
  },
  gradiant: {
    // background: '-webkit-gradient(linear, 121deg, #e83866 - 28 %, #e83866 - 21 %, #e57a3c 55 %, #e57a3c 75 %)',
    background: "-webkit-gradient(linear, left top, right top, from(#e83866), color-stop(-28%, #e83866), color-stop(55%, #e57a3c), color-stop(75%, #e57a3c))",
    backgroundColor: transperansy,
    color: "#fffff"
  }
};




export default badgeStyle;
