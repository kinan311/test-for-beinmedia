const customTabsStyle = {
  cardTitle: {
    float: "left",
    padding: "10px 10px 10px 0px",
    lineHeight: "24px"
  },
  cardTitleRTL: {
    float: "right",
    padding: "10px 0px 10px 10px !important"
  },
  displayIndicator: {
    display: "block !important",
    width: '25px !important',
    backgroundColor: 'white !important',
  },
  tabsRoot: {
    minHeight: "unset !important",
    width:"46%"
  },
  tabRootButton: {
    minHeight: "unset !important",
    minWidth: "unset !important",
    width: "unset !important",
    height: "unset !important",
    maxWidth: "unset !important",
    maxHeight: "unset !important",
    padding: "10px 10px",
    borderRadius: "3px",
    lineHeight: "24px",
    border: "0 !important",
    color: "#8191b2",
    marginLeft: "1vw",
    
    fontWeight: "500",
    fontSize: "14px",
    fontFamily: 'Loew Next Arabic',
    opacity: '1 !important',
    "&:last-child": {
      marginLeft: "0px"
    },
    "@media (max-width: 576px)": {
        fontSize: "3vw",
        marginLeft: "0.3vw",
    },
  },

  hideIcon:{
    display: "none"
  },
  tabRootButtonOpened: {
    minHeight: "unset !important",
    minWidth: "unset !important",
    width: "unset !important",
    height: "unset !important",
    maxWidth: "unset !important",
    maxHeight: "unset !important",
    padding: "10px 15px",
    borderRadius: "3px",
    lineHeight: "24px",
    border: "0 !important",
    color: "#fff !important",
    marginLeft: "0vw",
    fontWeight: "500",
    fontSize: "0.9vw",
    fontFamily: 'Loew Next Arabic',
    opacity: '1',
    "&:last-child": {
      marginLeft: "0px"
    }
  },
  extraContainer:{
    display: "flex",
    justifyContent: "space-around",
    width: "35%"
  },
  label: {
    fontSize: "14px",
    margin: "0px !important"   
  },
  value:{
    fontSize: "12px",
    margin: "0px !important",
    color: '#a7b5d2'
  },
  tabSelected: {
    backgroundColor: "rgba(255, 255, 255, 0)",
    transition: "0.2s background-color 0.1s",
    fontWeight: 'bold',
    color: '#ffffff !important',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'baseline'
  },
  tabWrapper: {
    display: "contents",
    minHeight: "unset !important",
    minWidth: "unset !important",
    width: "unset !important",
    height: "unset !important",
    maxWidth: "unset !important",
    maxHeight: "unset !important",
    "& > svg": {
      verticalAlign: "middle",
      margin: "-1.55px 5px 0 0 !important"
    },
    "&,& *": {
      letterSpacing: "normal !important"
    }
  }
};

export default customTabsStyle;
