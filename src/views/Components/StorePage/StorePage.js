import React, { Fragment } from "react";
import CustomCardList from '../../../components/CustomCardList/CusromCardList';
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import { makeStyles } from "@material-ui/core/styles";
import styles from "assets/jss/material-kit-react/components/typographyStyle.js";
import CustomPlayer from "components/CustomVideoPlayer/CustomPlayer";
import team1 from "../../../assets/img/faces/faceCard.png";
import team2 from "../../../assets/img/svgBG/videoBG2.png";
import team3 from "../../../assets/img/svgBG/videoBG3.png";
import team4 from "../../../assets/img/svgBG/videoBG4.png";
import CustomMobilePlayer from "components/CustomVideoPlayer/CustomMobilePlayer";



const useStyles = makeStyles(styles);

export default function StorePage(props) {
    const classes = useStyles();
    const [mobileView , setMobileView ] = React.useState(false)

    const [course, setCourse] = React.useState([
        { courseText: 'تعرف على أسرار صناعة المحتوى التسويقي الفعال', key: '1', imgSrc: team1, courseSalary: '299 دينار كويتي', url: 'https://www.youtube.com/watch?v=I4wp0aHDQ5Q&feature=youtu.be'},
        { courseText: 'تعرف على أسرار صناعة المحتوى التسويقي الفعال', key: '2', imgSrc: team2, courseSalary: '340 دينار كويتي', url: 'https://www.youtube.com/watch?v=-1C760hhwiQ'},
        { courseText: 'دورة صناعة المحتوى مع هند الناهض', key: '3', imgSrc: team3, courseSalary: '500 دينار كويتي', url: 'https://www.youtube.com/watch?v=1fq3W3z3bDg'},
        { courseText: ' دورة صناعة المحتوى مع هند الناهض', key: '4',fileType: 'PDF',  courseSalary: '190 دينار كويتي', url: 'https://www.youtube.com/watch?v=y7q5ds0Nqa0'},
        { courseText: 'تعرف على أسرار صناعة المحتوى', key: '5', fileType: 'Word',  courseSalary: '299 دينار كويتي', url: 'https://www.youtube.com/watch?v=VZokXw9B_b4'},
        { courseText: ' تعرف على أسرار صناعة المحتوى ', key: '6', fileType: 'Audio', courseSalary: '299 دينار كويتي', url: 'https://www.youtube.com/watch?v=iLXY1_CxhBE&list=PLXifkSAkNsF_z-C2kcAElW-uuCQ6jHxBm'},
    ]);

    const [selectedCourse, setSelectedCourse] = React.useState(course[0]);

    const changeView = (id) => {
    course.map(item => {
        if (item.key === id){
           setSelectedCourse(item)
        }
        });
    }

    const MobilePlayer = () => {
        if (props.isMobile === true) {
            return (
                <GridItem xs={12} sm={12} md={7} >
                    <CustomMobilePlayer url={selectedCourse.url} isMobile={props.isMobile} />
                </GridItem>

            )
        }
    }

    const WebPlayer = () => {
        if (props.isMobile === false) {
            return (

                <GridItem xs={12} sm={12} md={7} >
                    <CustomPlayer url={selectedCourse.url} courseText={selectedCourse.courseText}  isWeb={props.isWeb} />
                </GridItem>

            )
        }
    }
   
     return (
     
        <GridContainer justify="space-between"  >
            {MobilePlayer()}
          
            <GridItem xs={12} sm={12} md={5} className={classes.cardContainer}>
                {course.map((course) => (
                   
                    <CustomCardList courseList={false} key={course.key} imgSrc={course.imgSrc} fileType={course.fileType} courseText={course.courseText} courseSalary={course.courseSalary}  onClick={() => changeView(course.key)} />
                   
                ))}


            </GridItem>


            {WebPlayer()}
            
        </GridContainer>
       
    )
}