import React from "react";

import CustomCardList from '../../../components/CustomCardList/CusromCardList';
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";

import { makeStyles } from "@material-ui/core/styles";

import styles from "assets/jss/material-kit-react/components/typographyStyle.js";
import CustomPlayer from "components/CustomVideoPlayer/CustomPlayer";
import team1 from "../../../assets/img/faces/faceCard.png";
import team2 from "../../../assets/img/svgBG/videoBG2.png";
import team3 from "../../../assets/img/svgBG/videoBG3.png";
import team4 from "../../../assets/img/svgBG/videoBG4.png";
import CustomMobilePlayer from "components/CustomVideoPlayer/CustomMobilePlayer";
const useStyles = makeStyles(styles);

export default function BroadcastPage(props) {
    const classes = useStyles();

    const [course, setCourse] = React.useState([
        { courseText: 'تعرف على أسرار صناعة المحتوى التسويقي الفعال', id: '1', imgSrc: team1, courseSalary: '299 دينار كويتي' },
        { courseText: 'تعرف على أسرار صناعة المحتوى التسويقي الفعال', id: '2', imgSrc: team2, courseSalary: '340 دينار كويتي' },
        { courseText: 'دورة صناعة المحتوى مع هند الناهض', id: '3', imgSrc: team3, courseSalary: '500 دينار كويتي' },
        { courseText: ' دورة صناعة المحتوى مع هند الناهض', id: '4', imgSrc: team4, courseSalary: '190 دينار كويتي' },
        { courseText: 'فوكس حلقة ٥', id: '5', imgSrc: team1, courseSalary: '299 دينار كويتي' },
        { courseText: ' تعرف على أسرار صناعة المحتوى التسويقي الفعال', id: '6', imgSrc: team2, courseSalary: '299 دينار كويتي' },
        { courseText: ' دورة صناعة المحتوى مع هند الناهض', id: '4', imgSrc: team4, courseSalary: '190 دينار كويتي' },
        { courseText: 'فوكس حلقة ٥', id: '5', imgSrc: team3, courseSalary: '299 دينار كويتي' },
        { courseText: ' تعرف على أسرار صناعة المحتوى التسويقي الفعال', id: '6', imgSrc: team2, courseSalary: '299 دينار كويتي' },
        { courseText: ' دورة صناعة المحتوى مع هند الناهض', id: '4', imgSrc: team1, courseSalary: '190 دينار كويتي' },
        { courseText: 'فوكس حلقة ٥', id: '5', imgSrc: team4, courseSalary: '299 دينار كويتي' },
        { courseText: ' تعرف على أسرار صناعة المحتوى التسويقي الفعال', id: '6', imgSrc: team3, courseSalary: '299 دينار كويتي' },
        { courseText: ' دورة صناعة المحتوى مع هند الناهض', id: '4', imgSrc: team2, courseSalary: '190 دينار كويتي' },
        { courseText: 'فوكس حلقة ٥', id: '5', imgSrc: team1, courseSalary: '299 دينار كويتي' },
        { courseText: ' تعرف على أسرار صناعة المحتوى التسويقي الفعال', id: '6', imgSrc: team4, courseSalary: '299 دينار كويتي' },

    ])


    const MobilePlayer = () => {
        if (props.isMobile === true) {
            return (

                <GridItem xs={12} sm={12} md={7} >
                    <CustomMobilePlayer url='https://www.youtube.com/watch?v=I4wp0aHDQ5Q&feature=youtu.be' isMobile={props.isMobile} />
                </GridItem>

            )
        }
    }

    const WebPlayer = () => {
        if (props.isMobile === false) {
            return (

                <GridItem xs={12} sm={12} md={7} >
                    <CustomPlayer url='https://www.youtube.com/watch?v=I4wp0aHDQ5Q&feature=youtu.be' isWeb={props.isWeb} />
                </GridItem>

            )
        }
    }


    return (
        <GridContainer justify="space-between"  >

        {MobilePlayer()}

            <GridItem xs={12} sm={12} md={5} className={classes.cardContainer}>
                {course.map((course) => (
                    <CustomCardList courseList={false} imgSrc={course.imgSrc} courseText={course.courseText} courseSalary={course.courseSalary} />
                ))}


            </GridItem>

                 
          {WebPlayer()}
                  
        </GridContainer>
    )
}