import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// nodejs library to set properties for components
import PropTypes from "prop-types";

// material-ui components
import { makeStyles } from "@material-ui/core/styles";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Icon from "@material-ui/core/Icon";
// core components
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardHeader from "components/Card/CardHeader.js";

import styles from "assets/jss/material-kit-react/components/customTabsStyle.js";
import Button from "components/CustomButtons/Button.js";
const useStyles = makeStyles(styles);

export default function CustomTabs({hideIcon , ...props}) {
  const [value, setValue] = React.useState(0);

  const handleChange = (event, value) => {
      setValue(value); 
  };
  const classes = useStyles();
  const { headerColor, plainTabs, tabs, title, rtlActive } = props;
  const cardTitle = classNames({
    [classes.cardTitle]: true,
    [classes.cardTitleRTL]: rtlActive
  });
  const [tapStatus , setTabStatus] = React.useState(false)
  const navigationBack = () => {
    setTabStatus(true)
    hideIcon()
  }
  return (
    <Card plain={plainTabs}>
      <CardHeader color={headerColor} plain={plainTabs}>
        {title !== undefined ? <div className={cardTitle}>{title}</div> : null}
        <Tabs
          value={value}
          onChange={props.handleChange ? handleChange : ''}
          classes={{
            root: classes.tabsRoot,
            indicator: classes.displayIndicator
          }}
        >
          {tabs.map((prop, key) => {
            let icon = {};
            let color = {};
            let tabKey = 1;
            if (prop.tabIcon) {
              icon = {
                icon: <div style={prop.styleContainerIcon}><i class={props.viewIcon === true ?  prop.tabIcon.name : ' '} style={ prop.styleIcon} onClick={navigationBack}></i></div>
              };
            }
            if(prop.tabKey){
              setValue(prop.tabKey)
              
            }
            return (
              <Tab
                classes={{
                  root: props.isOpen ? classes.tabRootButtonOpened : classes.tabRootButton,
                  label: classes.tabLabel,
                  selected: classes.tabSelected,
                  wrapper: classes.tabWrapper,
                }}
                key={key}
                value={key}
                disabled={prop.disabled}
                label={prop.tabName}
                {...icon}
              />
            );
          })}
        </Tabs>

        <div style={{margin: '0 9%'}}></div>

          <div className={classes.extraContainer}>
              <div style={{ display: "flex",
                           justifyContent: "center",
                          alignItems: 'center'}}>
            <i class="fas fa-microphone fa-1x" style={
             {paddingRight: '20%', color: '#1dd950', background: 'white', padding: '10px', borderRadius: '50%', border: '3px solid #1dd950', margin: '0px 5px 0px 5px'}
            }></i>
            <div style={classes.labelContainer}>
              <p className={classes.label}>تجربة الصوت</p>
              <p className={classes.value}>جيد جداً</p>
            </div>
                         
          </div>

         <div style={{ display: "flex",
                           justifyContent: "center",
                          alignItems: 'center'}}>
            <i class="fas fa-video fa-1x" style={
             {paddingRight: '20%', color: '#e21956', background: 'white', padding: '10px', borderRadius: '50%', border: '3px solid #e21956', margin: '0px 5px 0px 5px'}
            }></i>
            <div >
              <p className={classes.label}> تجربة الكاميرا</p>
             <p className={classes.value}>لم تقم بإختيارها</p>
            </div>
                         
          </div>


          <div style={{ display: "flex",
                           justifyContent: "center",
                          alignItems: 'center'}}>
            <i class="fas fa-wifi fa-1x" style={
             {paddingRight: '20%', color: '#e28519', background: 'white', padding: '10px', borderRadius: '50%', border: '3px solid #e28519', margin: '0px 5px 0px 5px'}
            }></i>
                     <div style={classes.labelContainer}>
                        <p className={classes.label}>سرعة الأنترنت</p>
                         <p className={classes.value}>36 Mbps</p>
                     </div>   
          </div>
          </div>
        

      </CardHeader>
      <CardBody>
        {tabs.map((prop, key) => {
          if (key === value) {
            return <div key={key}>{prop.tabContent}</div>;
          }
          return null;
        })}
      </CardBody>
    </Card>
  );
}

CustomTabs.propTypes = {
  headerColor: PropTypes.oneOf([
    "warning",
    "success",
    "danger",
    "info",
    "primary",
    "rose"
  ]),
  title: PropTypes.string,
  tabs: PropTypes.arrayOf(
    PropTypes.shape({
      tabName: PropTypes.string.isRequired,
      tabIcon: PropTypes.object,
      tabContent: PropTypes.node.isRequired
    })
  ),
  rtlActive: PropTypes.bool,
  plainTabs: PropTypes.bool
};
