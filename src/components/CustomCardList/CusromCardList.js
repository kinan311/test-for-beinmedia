import React from "react";
import classNames from "classnames";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import styles from "assets/jss/material-kit-react/components/cardStyle.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Card from "components/Card/Card.js";
import videoIcon from "assets/img/svgBG/videoIcon.svg";
import fileIcon from "assets/img/fileIcon.svg";
import audioIcon from "assets/img/audioIcon.svg";
import overlay from "assets/img/overlay.svg";

const useStyles = makeStyles(styles);




export default function CustomCardList(props ) {
    const classes = useStyles();
    const { className, children, plain, carousel,id , ...rest } = props;
    const cardClasses = classNames({
        [classes.card]: true,
        [classes.cardPlain]: plain,
        [classes.cardCarousel]: carousel,
      
        [className]: className !== undefined
    });

    const imageClasses = classNames(
        classes.imgRaised,
        classes.imgCard,
        classes.imgFluid
    );

    const fileViewer = () => {
            switch (props.fileType) {
                               case 'PDF':
                                   return  (  <GridItem xs={12} sm={12} md={12} className={classes.itemGrid}>
                                        <div className={classes.fileViewer} ></div>
                                        <span style={{display: 'inline-flex' , justifyContent: 'center'}}>
                                        <p className={classes.fileLabel}>PDF</p>
                                        <img src={fileIcon} className={classes.iconFLoat} />
                                        </span>
                                        <img src={overlay} className={classes.Bgoverlay} />
                                    </GridItem>)
                                   break;
                           
                                

                                    case 'Audio':
                                    return ( <GridItem xs={12} sm={12} md={12} className={classes.itemGrid}>
                                            <div className={classes.fileViewer} ></div>
                                            <span style={{display: 'inline-flex' , justifyContent: 'center'}}>
                                            <p className={classes.fileLabel}>Audio</p>
                                            <img src={audioIcon} className={classes.iconFLoat} />
                                            </span>
                                            <img src={overlay} className={classes.Bgoverlay} />
                                        </GridItem>)
                                       
                                    break;

                                       case 'Word':
                                    return ( <GridItem xs={12} sm={12} md={12} className={classes.itemGrid}>
                                            <div className={classes.fileViewer} ></div>
                                            <span style={{display: 'inline-flex' , justifyContent: 'center'}}>
                                            <p className={classes.fileLabel}>Word</p>
                                            <img src={fileIcon} className={classes.iconFLoat} />
                                            </span>
                                            <img src={overlay} className={classes.Bgoverlay} />
                                        </GridItem>)
                                       

                                    break;
                               default:
                                   break;
                           }
    }


    return (

    
        <div className={props.containerStyle === 'CoursesContainer' ? classes.CoursesContainer : classes.container} onClick={props.onClick} >
                    <Card custom>
                        {props.imgSrc ?  
                        <GridItem xs={12} sm={12} md={12} className={classes.itemGrid}>
                           <img src={props.imgSrc} alt="..." className={imageClasses} />
                            <img src={videoIcon} className={classes.iconFLoat} />
                            <img src={overlay} className={classes.Bgoverlay} />
                         </GridItem>
                           : 
                           fileViewer()
                           } 
                       
                        <h4 className={classes.cardTitle}>
                                {props.courseText}
                            <br />
                            {props.courseList ? 

                                <small className={props.containerStyle === 'CoursesContainer' ?
                     classes.customSmallTitle :
                      classes.smallTitle}> <h4 style={{fontSize: '19px' , margin:'0 3px' , fontWeight:'bold'}}>{props.courseSalary + '  ' + '  '}</h4><h6 style={{fontSize: '13px'}}>{props.coins}</h6>
                       </small>

                            :
                              <small className={props.containerStyle === 'CoursesContainer' ?
                     classes.customSmallTitle :
                      classes.smallTitle}> {props.courseSalary + '  ' + '  '} 
                      <i class="fas fa-download" style={{color: '#844438',  margin:'0px 8%', display: 'inline-flex' , direction:'ltr'}}>  <p style={{    color: '#8191b2', margin: '0% 25%'}}> 18 </p> </i>
                       </small>
                       
                        }
                        </h4>
                    </Card>
            </div>
    );
}

CustomCardList.propTypes = {
    className: PropTypes.string,
    plain: PropTypes.bool,
    carousel: PropTypes.bool,
    children: PropTypes.node
};
