import React from "react";

import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";

import styles from "assets/jss/material-kit-react/components/headerLinksStyle.js";

import userImage from "assets/icons/user.png";
import userImageActive from "assets/icons/userActive.png";

import userBg from "../../assets/img/svgBG/userIcon.png"

import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import Divider from '@material-ui/core/Divider';

import ListItemIcon from '@material-ui/core/ListItemIcon';
import Badge from "components/Badge/Badge.js";
import Button from "components/CustomButtons/Button.js";

const useStyles = makeStyles(styles);


export default function MenuLinks(props) {

    



    const classes = useStyles();
    const [state, setState] = React.useState({
        left: false,
     
    });

    const [opened, setOpen] = React.useState(true);
    const [user, setUser] = React.useState([
        {userName: 'مستخدم 1', watingTime: 'منذ 15 دقيقة', icon: 'not-active'}, 
        { userName: 'مستخدم 2', watingTime: 'منذ 13 دقيقة', icon: 'not-active' }, 
        { userName: 'مستخدم 3', watingTime: 'منذ 9 دقيقة', icon: 'not-active' }, 
        { userName: 'مالك محمد', watingTime: 'منذ 7 دقيقة', icon: 'active' }, 
        { userName: 'مستخدم 4', watingTime: 'منذ 20 دقيقة', icon: 'not-active' }, 
        { userName: 'مستخدم 5', watingTime: 'منذ 10 دقيقة', icon: 'not-active' }, 
        { userName: 'مستخدم 6', watingTime: 'منذ 2 دقيقة', icon: 'not-active' }, 
       
]);

    const toggleDrawer = (anchor, open) => (event) => {
        if (event && event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }
        setOpen(!opened);
        setState({ ...state, [anchor]: open });
        props.open(opened);
    };

    

    const list = (anchor) => (
        <div
            className={clsx(classes.listMenu, {
                [classes.fullList]: anchor === 'top' || anchor === 'bottom',
            })}
            role="presentation"
            onClick={toggleDrawer(anchor, false)}
            onKeyDown={toggleDrawer(anchor, false)}
        >
            <h4 style={{ textAlign: 'center', fontWeight: '800' }} > ١٤ قائمة الإنتظار  </h4>
            <Divider className={classes.devider}></Divider>
            <List className={classes.usersContainer}>
                {user.map((user) => (
                   
                    <ListItem button key={user.userName} className={classes.itemContainer}>
                        <ListItem className={classes.userName}  ><h6 className={classes.time}>{user.watingTime}</h6>{user.userName}</ListItem>
                        <ListItemIcon className={classes.bgIcon}>{user.icon === 'active' ? <div style={{  }}>
                             <img src={userImageActive}>
                                 </img></div> 
                                 : <div><img src={userImage}>
                            </img></div>}</ListItemIcon>

                    </ListItem>
               ))}
            </List>
            <Divider />
            <div className={classes.privacyContainer}>
                <div className={classes.privacyChild}>
                    <p style={{ color: '#889dc7'}}> جميع الحقوق محفوظة تايم فيوير @ 2013-2020</p>
            <div className={classes.socialLine}>
                <Button
                    justIcon
                    href="#pablo"
                    target="_blank"
                    color="transparent"
                    onClick={e => e.preventDefault()}
                            style={{ color: '#889dc7' }}
                >
                    <i className={classes.socialIcons + " fab fa-twitter"} />
                </Button>
                <Button
                    justIcon
                    href="#pablo"
                    target="_blank"
                    color="transparent"
                    onClick={e => e.preventDefault()}
                            style={{ color: '#889dc7' }}
                >
                    <i className={classes.socialIcons + " fab fa-facebook"} />
                </Button>
                <Button
                    justIcon
                    href="#pablo"
                    target="_blank"
                    color="transparent"
                    onClick={e => e.preventDefault()}
                            style={{ color: '#889dc7' }}
                >
                    <i
                        className={
                            classes.socialIcons + " fab fa-instagram"
                        }
                    />
                </Button>
            </div>
                </div>
            </div>
        </div>
    );
    return (
        <div className={classes.menuContainer}>
            {['left'].map((anchor) => (
                <React.Fragment key={anchor}>
                    {!opened  ? 
                     <i className="fas fa-arrow-left" style={{display: 'inline-flex', alignItems: 'center'}} onClick={toggleDrawer(anchor, opened)}></i>
                    :
                    <div className={classes.menuContainer}>
                    <i className="fas fa-arrow-right" style={{display: 'inline-flex', alignItems: 'center', marginLeft: '7%'}} onClick={toggleDrawer(anchor, opened)}></i>
                    <Badge color="transperansy" className={classes.Badge}>١٤</Badge>
                    <Button color="primary" transparent="true" simple onClick={toggleDrawer(anchor, opened)} className={classes.menuBtn}>قائمة الإنتظار</Button>
                    </div>
         
                      }
                   
                    <SwipeableDrawer
                        anchor={anchor}
                        open={state[anchor]}
                        onClose={toggleDrawer(anchor, false)}
                        onOpen={toggleDrawer(anchor, true)}
                        className={classes.menuStyle}
                    >
                        {list(anchor)}
                    </SwipeableDrawer>
                </React.Fragment>
            ))}
        </div>
    )
}
