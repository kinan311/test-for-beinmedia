import React from "react";

import classNames from "classnames";

import PropTypes from "prop-types";

import { makeStyles } from "@material-ui/core/styles";
import styles from "assets/jss/material-kit-react/components/cardStyle.js";

import ReactPlayer from 'react-player'

const useStyles = makeStyles(styles);


export default function CustomMobilePlayer(props) {
    const classes = useStyles();
    const { className, children, plain, carousel, ...rest } = props;
    const cardClasses = classNames({
        [classes.card]: true,
        [classes.cardPlain]: plain,
        [classes.cardCarousel]: carousel,
        [className]: className !== undefined
    });

   
    return (

        <ReactPlayer
            className={classes.videoPlayer}
            controls={false}
            width="100%"
            height= "40vh"
            url={props.url}
            config={{
                youtube: {
                    playerVars: { showinfo: 0 }
                }
            }}
        />



    );
}

CustomMobilePlayer.propTypes = {
    className: PropTypes.string,
    plain: PropTypes.bool,
    carousel: PropTypes.bool,
    children: PropTypes.node
};
