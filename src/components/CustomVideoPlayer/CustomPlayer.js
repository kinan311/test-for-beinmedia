import React from "react";

import classNames from "classnames";

import PropTypes from "prop-types";

import { makeStyles } from "@material-ui/core/styles";
import styles from "assets/jss/material-kit-react/components/cardStyle.js";

import ReactPlayer from 'react-player'

import overlay from "../../assets/img/overlay.svg"
import videoBg from "../../assets/img/svgBG/videoBG.png"
const useStyles = makeStyles(styles);


export default function CustomPlayer(props) {
    const classes = useStyles();
    const { className, children, plain, carousel, ...rest } = props;
    const cardClasses = classNames({
        [classes.card]: true,
        [classes.cardPlain]: plain,
        [classes.cardCarousel]: carousel,
        [className]: className !== undefined
    });



    return (
        <React.Fragment>
            
        <ReactPlayer
            className={classes.videoPlayer}
            controls={false}
            width="100%"
            height="70vh"
            url={props.url}
            config={{
                mixcloud: {
                },
                youtube: {
                    playerVars: { showinfo: false }
                }
            }}
        />
            {/* <span className={classes.bgOverlay}></span> */}
            <img src={overlay} alt="..." className={classes.bgOverlay}/>

             <span className={classes.overLabel}>
                {props.courseText ? props.courseText : 'المقدمة التعريفية ل هند الناهض'}
            </span>
           
       
        </React.Fragment>
        


       
    );

  
}


CustomPlayer.propTypes = {
    className: PropTypes.string,
    plain: PropTypes.bool,
    carousel: PropTypes.bool,
    children: PropTypes.node
};